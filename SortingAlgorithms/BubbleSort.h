#ifndef BUBBLESORT_H_INCLUDED
#define BUBBLESORT_H_INCLUDED
#include<vector>
class BubbleSort
{
public:
    BubbleSort();
    ~BubbleSort();
    void BubbleSorter(int Numbers[] ,int array_size);
    void Print(int Numbers[],int array_size);
    void BubbleSorterVector(std::vector<int> Numbers);
    void PrintVector(std::vector<int> Numbers);

};


#endif // BUBBLESORT_H_INCLUDED
