#ifndef MERGESORT_H_INCLUDED
#define MERGESORT_H_INCLUDED
#include<vector>
class MergeSort
{
public:
    MergeSort();
    ~MergeSort();
    void MergeSorter(int Numbers[], int array_size);
    void MergeSorterVector(std::vector<int> Numbers, int array_size);
    void Print(int Numbers[], int array_size);
    void Print(std::vector<int> Numbers);
};

#endif // MERGESORT_H_INCLUDED
