#include"BubbleSort.h"
#include<iostream>
#include<iterator>
using namespace std;

BubbleSort::BubbleSort()
{

}
BubbleSort::~BubbleSort()
{

}

void BubbleSort::BubbleSorter(int Numbers [], int array_size)
{
    bool swapped=true;
    int j=0;
    int temp;
    while(swapped)
    {
        swapped=false;
        j++;
        for(int i=0;i<array_size-j;++i)
        {
            if(Numbers[i]>Numbers[i+1])
            {
                temp=Numbers[i];
                Numbers[i]=Numbers[i+1];
                Numbers[i+1]=temp;
                swapped=true;
            }
        }
    }
}

void BubbleSort::Print(int Numbers[],int array_size)
{
    for(int i=0;i<array_size;i++)
        cout<<Numbers[i]<<endl;
}

void BubbleSort::BubbleSorterVector(vector<int> Numbers)
{
    bool swapped=true;
    int j=0;
    int temp;
    while(swapped)
    {
        swapped=false;
        j++;
        for(int i=0;i<Numbers.size()-j;++i)
        {
            if(Numbers[i]>Numbers[i+1])
            {
                temp=Numbers[i];
                Numbers[i]=Numbers[i+1];
                Numbers[i+1]=temp;
                swapped=true;
            }
        }
    }
}
void BubbleSort::PrintVector(vector<int> Numbers)
{
    for(int i=0;i<Numbers.size();i++)
    {
        cout<<Numbers[i]<<endl;
    }
}
