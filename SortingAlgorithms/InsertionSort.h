#ifndef INSERTIONSORT_H_INCLUDED
#define INSERTIONSORT_H_INCLUDED
#include<vector>
class InsertionSort
{
public:
    InsertionSort();
    ~InsertionSort();
    void InsertionSorter(int Numbers[], int array_size);
    void Print(int Numbers[],int array_size);
    void InsertionSorterVector(std::vector<int> Numbers);
    void Print(std::vector<int> Numbers);
};

#endif // INSERTIONSORT_H_INCLUDED
