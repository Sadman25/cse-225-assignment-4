#include <iostream>
#include"SelectionSort.h"
#include"MergeSort.h"
#include"InsertionSort.h"
#include"BubbleSort.h"
#include<vector>
using namespace std;
using std::vector;

int main()
{
    int ArraySize;
    cout<<"Enter Array Size=";
    cin>>ArraySize;
    int DemoArray[ArraySize];
    cout<<"Enter array elements..."<<endl;
    for(int i=0;i<ArraySize;i++)
    {
        cin>>DemoArray[i];
    }
    SelectionSort demoSelect;
    demoSelect.SelectionSorter(DemoArray,ArraySize);
    cout<<"Sorted list after using Selection sort is given below.."<<endl;
    demoSelect.Print(DemoArray,ArraySize);
    cout<<"\n\nSorted list after using Bubble sort is given below.."<<endl;
    BubbleSort demoBubble;
    demoBubble.BubbleSorter(DemoArray,ArraySize);
    demoBubble.Print(DemoArray,ArraySize);
    cout<<"\n\nSorted list after using Insertion sort is given below.."<<endl;
    InsertionSort DemoInsert;
    DemoInsert.InsertionSorter(DemoArray,ArraySize);
    DemoInsert.Print(DemoArray,ArraySize);
    MergeSort DemoMerge;
    DemoMerge.MergeSorter(DemoArray,ArraySize);
    cout<<"\n\nSorted list after using Merge sort is given below.."<<endl;
    DemoMerge.Print(DemoArray,ArraySize);

    vector<int> DemoVector;
    DemoVector.push_back(12);
    DemoVector.push_back(5);
    DemoVector.push_back(45);
    DemoVector.push_back(14);

    cout<<"\n\nSorted list after using Bubble sort is given below.."<<endl;
    BubbleSort demoBubbleVector;
    demoBubbleVector.BubbleSorterVector(DemoVector);
    demoBubbleVector.PrintVector(DemoVector);

    cout<<"\n\nSorted list after using Insertion sort is given below.."<<endl;
    InsertionSort DemoInsertionVector;
    DemoInsertionVector.InsertionSorterVector(DemoVector);
    DemoInsertionVector.Print(DemoVector);

    SelectionSort demoSelectionVector;
    demoSelectionVector.SelectionSorterVector(DemoVector);
    demoSelectionVector.Print(DemoVector);

    cout<<"\n\nSorted list after using Merge sort is given below.."<<endl;
    MergeSort DemoMergeVector;
    DemoMergeVector.MergeSorterVector(DemoVector,DemoVector.size());
    DemoMergeVector.Print(DemoVector);


}
