#include"InsertionSort.h"
#include<iostream>
using namespace std;
InsertionSort::InsertionSort()
{

}
InsertionSort::~InsertionSort()
{

}
void InsertionSort::InsertionSorter(int Numbers[],int array_size)
{
    if(array_size>1)
    {
        int Size=array_size;
        for(int i=1;i<Size;++i)
        {
            int j=i-1;
            int key=Numbers[i];
            while(j>=0 && Numbers[j]>key)
            {
                Numbers[j+1]=Numbers[j];
                --j;
            }
            Numbers[j+1]=key;
        }
    }
}

void InsertionSort::Print(int Numbers[],int array_size)
{
    for(int i=0;i<array_size;i++)
        cout<<Numbers[i]<<endl;
}

void InsertionSort::InsertionSorterVector(vector<int> Numbers)
{
    if(Numbers.size()>1)
    {
        int Size=Numbers.size();
        for(int i=1;i<Size;++i)
        {
            int j=i-1;
            int key=Numbers[i];
            while(j>=0 && Numbers[j]>key)
            {
                Numbers[j+1]=Numbers[j];
                --j;
            }
            Numbers[j+1]=key;
        }
    }
}
void InsertionSort::Print(vector<int> Numbers)
{
    for(int i=0;i<Numbers.size();i++)
    {
        cout<<Numbers[i]<<endl;
    }
}
