#include"SelectionSort.h"
#include<iostream>
using namespace std;
SelectionSort::SelectionSort()
{

}
SelectionSort::~SelectionSort()
{


}

inline void Swap(int & a, int &b)
{
    int k=a;
    a=b;
    b=k;
}
void SelectionSort::SelectionSorter(int Numbers[], int array_size)
{
    for(int i=0;i<array_size-1;++i)
    {
        int Min=i;
        for(int j=i+1;j<array_size;++j)
        {
            if(Numbers[j]<Numbers[Min])
                Min=j;
            Swap(Numbers[Min],Numbers[i]);
        }
    }
}

void SelectionSort::Print(int Numbers[],int array_size)
{
    for(int i=0;i<array_size;i++)
        cout<<Numbers[i]<<endl;
}

void SelectionSort::SelectionSorterVector(vector<int> Numbers)
{
    for(int i=0;i<Numbers.size()-1;++i)
    {
        int Min=i;
        for(int j=i+1;j<Numbers.size();++j)
        {
            if(Numbers[j]<Numbers[Min])
                Min=j;
            Swap(Numbers[Min],Numbers[i]);
        }
    }
}
void SelectionSort::Print(vector<int> Numbers)
{
    for(int i=0;i<Numbers.size();i++)
    {
        cout<<Numbers[i]<<endl;
    }
}
