#ifndef SELECTIONSORT_H_INCLUDED
#define SELECTIONSORT_H_INCLUDED
#include<vector>

class SelectionSort
{
public:
    SelectionSort();
    ~SelectionSort();
    void SelectionSorter(int Numbers[] , int array_size);
    void Print(int Numbers[],int array_size);
    void SelectionSorterVector(std::vector <int> Numbers);
    void Print(std::vector<int> Numbers);
};


#endif // SELECTIONSORT_H_INCLUDED
