#include"MergeSort.h"
#include<iostream>
using namespace std;
MergeSort::MergeSort()
{

}
MergeSort::~MergeSort()
{

}
void Merger(int Numbers[],int low, int mid, int high)
{
    int *temp=new int[high-low+1];
    int i=low;
    int j=mid+1;
    int k=0;
    while(i<=mid && j<=high)
    {
        if(Numbers[i]<=Numbers[j])
            temp[k++]=Numbers[i++];
        else
            temp[k++]=Numbers[j++];
    }
    while(i<=mid)
        temp[k++]=Numbers[i++];
    while(j<=high)
        temp[k++]=Numbers[j++];
    for(k=0,i=low;i<=high;++i,++k)
        Numbers[i]=temp[k];

    delete [] temp;

}

void MergeSortHelper(int Numbers[], int low,int high)
{
    int mid;
    if(low<high)
    {
        mid=(low+high)/2;
        MergeSortHelper(Numbers,low,mid);
        MergeSortHelper(Numbers,mid+1,high);
        Merger(Numbers,low,mid,high);
    }
}
void MergeSort::MergeSorter(int Numbers[],int array_size)
{
    MergeSortHelper(Numbers,0,array_size-1);
}
void MergeSort::Print(int Numbers[],int array_size)
{
    for(int i=0;i<array_size;i++)
        cout<<Numbers[i]<<endl;
}
void VectorMerger(std::vector<int> Numbers,int low, int mid, int high)
{
    int *temp=new int[high-low+1];
    int i=low;
    int j=mid+1;
    int k=0;
    while(i<=mid && j<=high)
    {
        if(Numbers[i]<=Numbers[j])
            temp[k++]=Numbers[i++];
        else
            temp[k++]=Numbers[j++];
    }
    while(i<=mid)
        temp[k++]=Numbers[i++];
    while(j<=high)
        temp[k++]=Numbers[j++];
    for(k=0,i=low;i<=high;++i,++k)
        Numbers[i]=temp[k];

    delete [] temp;

}
void VectorMergeSortHelper(std::vector<int> Numbers, int low,int high)
{
    int mid;
    if(low<high)
    {
        mid=(low+high)/2;
        VectorMergeSortHelper(Numbers,low,mid);
        VectorMergeSortHelper(Numbers,mid+1,high);
        VectorMerger(Numbers,low,mid,high);
    }
}
void MergeSort::MergeSorterVector(std::vector<int> Numbers,int array_size)
{
    VectorMergeSortHelper(Numbers,0,array_size-1);
}

void MergeSort::Print(vector<int> Numbers)
{
    for(int i=0;i<Numbers.size();i++)
    {
        cout<<Numbers[i]<<endl;
    }
}

